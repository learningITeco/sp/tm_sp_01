<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/headerp.jsp" />

<body>

	<div class="container">

		<c:if test="${not empty msg}">
		    <div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		    </div>
		</c:if>

		<h1>All Projects</h1>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>#ID</th>
					<th>NAME</th>
					<th>DESCRIPTION</th>
					<th>USER</th>
					<th>DATE START</th>
					<th>DATE FINISH</th>
					<th>STATUS</th>
				</tr>
			</thead>

			<c:forEach var="project" items="${projectList}">
			    <tr>
				<td>
					${project.id}
				</td>
				<td>${project.name}</td>
				<td>${project.description}</td>
				<td>${project.user.getLogin()}</td>
				<td>${project.dateStart}</td>
				<td>${project.dateFinish}</td>
				<td>${project.status}</td>

				<td>
				  <spring:url value="/projects/${project.id}/delete" var="deleteUrl" />
				  <spring:url value="/projects/${project.id}/update" var="updateUrl" />

				  <button class="btn btn-primary"
                                          onclick="location.href='${updateUrl}'">Update</button>
				  <button class="btn btn-danger"
                                          onclick="location.href='${deleteUrl}'">Delete</button>
                </td>
			    </tr>
			</c:forEach>
		</table>

	</div>
</body>
</html>