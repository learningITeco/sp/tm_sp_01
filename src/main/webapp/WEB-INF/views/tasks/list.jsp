<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/headert.jsp" />

<body>

	<div class="container">

		<c:if test="${not empty msg}">
		    <div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		    </div>
		</c:if>

		<h1>All Tasks</h1>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>#ID</th>
					<th>NAME</th>
					<th>DESCRIPTION</th>
					<th>PROJECT</th>
					<th>USER</th>
					<th>DATE START</th>
					<th>DATE FINISH</th>
					<th>STATUS</th>
				</tr>
			</thead>

			<c:forEach var="task" items="${taskList}">
			    <tr>
				<td>
					${task.id}
				</td>
				<td>${task.name}</td>
				<td>${task.description}</td>
				<td>${task.project}</td>
				<td>${task.user.getLogin()}</td>
				<td>${task.dateStart}</td>
				<td>${task.dateFinish}</td>
				<td>${task.status}</td>

				<td>
				  <spring:url value="/tasks/${task.id}/delete" var="deleteUrl" />
				  <spring:url value="/tasks/${task.id}/update" var="updateUrl" />

				  <button class="btn btn-primary"
                                          onclick="location.href='${updateUrl}'">Update</button>
				  <button class="btn btn-danger"
                                          onclick="location.href='${deleteUrl}'">Delete</button>
                </td>
			    </tr>
			</c:forEach>
		</table>

	</div>
</body>
</html>