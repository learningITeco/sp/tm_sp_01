<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<body>

	<div class="container">

		<c:if test="${not empty msg}">
		    <div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		    </div>
		</c:if>

		<h1>All Users</h1>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>#ID</th>
					<th>LOGIN</th>
					<th>HASHPASS</th>
					<th>ROLE</th>
				</tr>
			</thead>

			<c:forEach var="user" items="${userList}">
			    <tr>
				<td>
					${user.id}
				</td>
				<td>${user.login}</td>
				<td>${user.hashPass}</td>
				<td>${user.roleType}</td>

				<td>
				  <spring:url value="/users/${user.id}/delete" var="deleteUrl" />
				  <spring:url value="/users/${user.id}/update" var="updateUrl" />

				  <button class="btn btn-primary"
                                          onclick="location.href='${updateUrl}'">Update</button>
				  <button class="btn btn-danger"
                                          onclick="location.href='${deleteUrl}'">Delete</button>
                </td>
			    </tr>
			</c:forEach>
		</table>

	</div>
</body>
</html>