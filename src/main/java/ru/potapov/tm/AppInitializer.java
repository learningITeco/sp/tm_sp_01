package ru.potapov.tm;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.util.WebMvcConfig;

@Getter
@Setter
@NoArgsConstructor
@ComponentScan(basePackages = "ru.potapov.tm")
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    private ServiceLocator serviceLocator;
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { };
    }
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebMvcConfig.class };
    }
    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

}
