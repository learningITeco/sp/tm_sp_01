package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_project")
public class Project extends AbstractProjectTask {
    //Check if this is for New of Update
    public boolean isNew() {
        return (getName().isEmpty());
    }

//    @Override
    public String toString() {
        return getName() + " (" + getStatus() + ") - " + getDescription();
    }
}
