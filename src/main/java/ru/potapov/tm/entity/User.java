package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.enumeration.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "app_user")
public class User extends AbstractEntity {

    @Column(nullable = false, unique = true)
    @Nullable private String login;

    @Column(name = "passwordHash")
    @Nullable private String hashPass;

    @Column(name = "session_id")
    @Nullable private String sessionId;

    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    @NotNull private RoleType roleType = RoleType.User;

    //Check if this is for New of Update
    public boolean isNew() {
        return (getLogin().isEmpty());
    }

    @Override
    public String toString() {
        return login + " [" + roleType + "]";
    }
}
