package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_task")
public class Task extends AbstractProjectTask{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="project_id")
    @Nullable
    private Project      project;

    //Check if this is for New of Update
    public boolean isNew() {
        return (getName().isEmpty());
    }

}
