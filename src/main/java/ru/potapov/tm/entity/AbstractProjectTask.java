package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.potapov.tm.enumeration.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class AbstractProjectTask extends AbstractEntity {
    @Column(name = "name", unique = true, nullable = false)
    @Nullable
    private String      name;

    @Column(name = "description")
    @Nullable private String      description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @Nullable private User  user;

    @Column(name = "dateBegin")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Nullable private Date dateStart;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "dateEnd")
    @Nullable private Date dateFinish;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    @Nullable private Status status;
}
