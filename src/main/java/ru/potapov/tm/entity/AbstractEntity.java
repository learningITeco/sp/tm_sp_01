package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Id;
import java.util.UUID;

@Setter
@Getter
public abstract class AbstractEntity {

    @Id
    @Nullable
    private String id;

    public AbstractEntity() {
        id = UUID.randomUUID().toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
