package ru.potapov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.potapov.tm.api.*;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;
import ru.potapov.tm.service.ProjectService;
import ru.potapov.tm.service.SessionService;
import ru.potapov.tm.service.TaskService;
import ru.potapov.tm.service.UserService;

import java.util.Date;

@Setter
@Getter
@Component
public class Bootstrap implements ServiceLocator {

    @Nullable User curUser;
    @Nullable Project curProject;
    boolean init = false;

    @Autowired
    @NotNull IProjectService projectService;

    @Autowired
    @NotNull ITaskService taskService;

    @Autowired
    @NotNull IUserService userService;

    @Autowired
    @NotNull ISessionService sessionService;

    public Bootstrap() { }

    public void init(){
        createUsers();
        createProjects();
        createTasks();
        init = true;
    }

    public void createUsers(){

        User user = new User();
        user.setLogin("admin");
        user.setRoleType(RoleType.Administrator);
        user.setHashPass("sdsdsdsd");
        getUserService().persist(user);
        curUser = user;
        
        user = new User();
        user.setLogin("user");
        user.setRoleType(RoleType.User);
        user.setHashPass("hjjjjjjjj");
        getUserService().persist(user);
    }
    public void createProjects(){

        Project project = new Project();
        project.setName("P1");
        project.setDescription("D1");
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        project.setStatus(Status.Planned);
        project.setUser(curUser);
        getProjectService().persist(project);
        curProject = project;

        project = new Project();
        project.setName("P2");
        project.setDescription("D2");
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        project.setStatus(Status.InProcess);
        project.setUser(curUser);
        getProjectService().persist(project);
    }
    public void createTasks(){
        
        Task task = new Task();
        task.setName("Task1");
        task.setProject(curProject);
        task.setDescription("Description T1");
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        task.setStatus(Status.Planned);
        task.setUser(curUser);
        getTaskService().persist(task);
    }
}
