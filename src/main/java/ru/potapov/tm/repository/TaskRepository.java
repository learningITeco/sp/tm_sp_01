package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Task;

@Getter
@Setter
@NoArgsConstructor
@Repository
public class TaskRepository extends AbstractRepository<Task> {
}
