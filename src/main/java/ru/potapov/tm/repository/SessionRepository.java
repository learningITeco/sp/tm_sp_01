package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Session;

@Getter
@Setter
@NoArgsConstructor
@Repository
public class SessionRepository extends AbstractRepository<Session> {
}
