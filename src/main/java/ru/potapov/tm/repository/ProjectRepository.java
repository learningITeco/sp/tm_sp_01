package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.entity.Project;

@Getter
@Setter
@NoArgsConstructor
@Repository
public class ProjectRepository extends AbstractRepository<Project>{

}
