package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.HashMap;

@Getter
@Setter
@NoArgsConstructor
@Repository
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    @NotNull HashMap<String, T> map = new HashMap<>();

    @NotNull
    @Override
    public T findOne(@NotNull final String id) {
        return map.get(id);
    }

    @Override
    public @NotNull Collection<T> findAll() {
        return map.values();
    }

    @Override
    public void merge(@NotNull final T t) {
        map.put(t.getId(), t);
    }

    @Override
    public void persist(@NotNull final T t) {
        map.put(t.getId(), t);
    }

    @Override
    public void remove(@NotNull final T t) {
        map.remove(t.getId());
    }

    @Override
    public void removeAll() {
        map.clear();
    }
}
