package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.repository.TaskRepository;

@Service
@Getter
@Setter
@Component
public class TaskService extends AbstractService<Task> implements ITaskService {
}
