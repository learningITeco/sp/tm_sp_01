package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.UserRepository;

@Service
@Getter
@Setter
@Component
public class UserService extends AbstractService<User> implements IUserService {
}
