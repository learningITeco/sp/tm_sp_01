package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.potapov.tm.api.ISessionService;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.repository.SessionRepository;

@Service
@Getter
@Setter
@Component
public class SessionService extends AbstractService<Session> implements ISessionService {
}
