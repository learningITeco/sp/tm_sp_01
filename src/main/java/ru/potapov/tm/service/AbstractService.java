package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.IService;
import ru.potapov.tm.entity.AbstractEntity;

import java.util.Collection;

@Service
@Getter
@Setter
@NoArgsConstructor
@Component
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Autowired
    @Nullable IRepository<T> repository;

    @NotNull
    @Override
    public T findOne(@NotNull final String id) {
        if (repository != null)
            return repository.findOne(id);
        return null;
    }

    @Override
    public @NotNull Collection<T> findAll() {
        if (repository != null)
            return repository.findAll();
        return null;
    }

    @Override
    public void merge(@NotNull final T t) {
        if (repository != null)
            repository.merge(t);
    }

    @Override
    public void persist(@NotNull final T t) {
        if (repository != null)
            repository.persist(t);
    }

    @Override
    public void remove(@NotNull final T t) {
        if (repository != null)
            repository.remove(t);
    }

    @Override
    public void removeAll() {
        if (repository != null)
            repository.removeAll();
    }
}
