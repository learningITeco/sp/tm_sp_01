package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.repository.ProjectRepository;

@Service
@Getter
@Setter
@Component
public class ProjectService extends AbstractService<Project> implements IProjectService {
}
