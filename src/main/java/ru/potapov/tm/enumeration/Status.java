package ru.potapov.tm.enumeration;

public enum Status implements Comparable<Status> {
    Planned("1"),
    InProcess("2"),
    Completed("3");

    private String prioritet;

    Status(String prioritet) {
        this.prioritet = prioritet;
    }

    public String displayName(){
        String res = "";
        switch (this){
            case Planned:   res = "Planned"; break;
            case InProcess: res = "In process"; break;
            case Completed: res = "Completed"; break;
        }

        return res;
    }
}
