package ru.potapov.tm.api;

import ru.potapov.tm.entity.Session;

public interface ISessionService extends IService<Session> {
}
