package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IRepository<T extends AbstractEntity> {
    @NotNull T findOne(@NotNull final String id);
    @NotNull Collection<T> findAll();
    void merge(@NotNull T t);
    void persist(@NotNull T t);
    void remove(@NotNull T t);
    void removeAll();
}
