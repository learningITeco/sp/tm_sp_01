package ru.potapov.tm.api;

import ru.potapov.tm.entity.User;

public interface IUserService extends IService<User> {
}
