package ru.potapov.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.potapov.tm.api.*;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import java.text.SimpleDateFormat;
import java.util.*;

@Setter
@Getter
@Controller
@ComponentScan(basePackages = "ru.potapov.tm")
public class MainController {

    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    @NotNull ServiceLocator serviceLocator;

    public MainController() {
        System.out.println(serviceLocator);
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(){
        if (serviceLocator == null)
            return null;

        if (!serviceLocator.isInit())
            serviceLocator.init();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("start");

        return modelAndView;
    }
    
    // list page
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String showAllUsers(Model model) {
        logger.debug("showAllUsers()");
        model.addAttribute("userList", serviceLocator.getUserService().findAll());
        return "users/list";
    }

    // list page
    @RequestMapping(value = "/project", method = RequestMethod.POST)
    public String showAllProjects(Model model) {
        logger.debug("showAllProjects()");
        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());
        return "projects/list";
    }
    // list page
    @RequestMapping(value = "/task", method = RequestMethod.POST)
    public String showAllTasks(Model model) {
        logger.debug("showAllTasks()");
        model.addAttribute("taskList", serviceLocator.getTaskService().findAll());
        return "tasks/list";
    }

    //users
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String saveOrUpdateUser(@ModelAttribute("userForm") @Validated @NotNull User user,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {
        logger.debug("saveOrUpdateUser() : {}", user);

        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "users/userform";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(user.isNew()){
                redirectAttributes.addFlashAttribute("msg", "User added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "User updated successfully!");
            }
            serviceLocator.getUserService().merge(user);

            // POST/FORWARD/GET
            populateDefaultModel(model);
            return "users/list";
        }
    }

    // show add user form
    @RequestMapping(value = "/users/add", method = RequestMethod.GET)
    public String showAddUserForm(Model model) {
        logger.debug("showAddUserForm()");

        @NotNull  User user = new User();
        // set default value
        user.setLogin("");
        user.setRoleType(RoleType.User);
        user.setHashPass("fdfdffdfdfdfdfdfdfs");
        model.addAttribute("userForm", user);

        populateDefaultModel(model);

        return "users/userform";
    }

    // show update form
    @RequestMapping(value = "/users/{id}/update", method = RequestMethod.GET)
    public String showUpdateUserForm(@PathVariable("id") String id, Model model) {
        logger.debug("showUpdateUserForm() : {}", id);

        @NotNull final User user = serviceLocator.getUserService().findOne(id);
        model.addAttribute("userForm", user);

        populateDefaultModel(model);

        return "users/userform";
    }

    // delete user
    @RequestMapping(value = "/users/{id}/delete", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes) {
        logger.debug("deleteUser() : {}", id);

        @NotNull final User user = serviceLocator.getUserService().findOne(id);
        if (user != null)
            serviceLocator.getUserService().remove(user);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "User is deleted!");

        populateDefaultModel(model);

        return "users/list";
    }

    //sessions
    @RequestMapping(value = "/sessions", method = RequestMethod.POST)
    public String saveOrUpdateSession(@ModelAttribute("sessionForm") @Validated @NotNull Session session,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {

        logger.debug("saveOrUpdateSession() : {}", session);
        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "sessions/sessionform";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(session.isNew()){
                redirectAttributes.addFlashAttribute("msg", "Session added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "Session updated successfully!");
            }
            serviceLocator.getSessionService().merge(session);

            populateDefaultModel(model);

            return "sessions/list";
        }
    }

    // show add session form
    @RequestMapping(value = "/sessions/add", method = RequestMethod.GET)
    public String showAddSessionForm(Model model) {

        logger.debug("showAddSessionForm()");

        @NotNull Session session = new Session();
        // set default value
        session.setSignature("");
        session.setDateStamp(new Date().getTime());
        session.setUser(null);
        model.addAttribute("sessionForm", session);

        populateDefaultModel(model);

        return "sessions/sessionform";
    }

    // show update form
    @RequestMapping(value = "/sessions/{id}/update", method = RequestMethod.GET)
    public String showUpdateSessionForm(@PathVariable("id") String id, Model model) {
        logger.debug("showUpdateSessionForm() : {}", id);

        @NotNull final Session session = serviceLocator.getSessionService().findOne(id);
        model.addAttribute("sessionForm", session);

        populateDefaultModel(model);

        return "sessions/sessionform";
    }

    // delete session
    @RequestMapping(value = "/sessions/{id}/delete", method = RequestMethod.GET)
    public String deleteSession(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes) {
        logger.debug("deleteSession() : {}", id);

        @NotNull final Session session = serviceLocator.getSessionService().findOne(id);
        if (session != null)
            serviceLocator.getSessionService().remove(session);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Session is deleted!");

        populateDefaultModel(model);

        return "sessions/list";        
    }

    //projects
    @RequestMapping(value = "/projects", method = RequestMethod.POST)
    public String saveOrUpdateProject(@ModelAttribute("projectForm") @Validated @NotNull Project project,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {

        logger.debug("saveOrUpdateProject() : {}", project);
        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "projects/projectform";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(project.isNew()){
                redirectAttributes.addFlashAttribute("msg", "Project added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "Project updated successfully!");
            }
            serviceLocator.getProjectService().merge(project);

            populateDefaultModel(model);

            return "projects/list";
        }
    }

    // show add project form
    @RequestMapping(value = "/projects/add", method = RequestMethod.GET)
    public String showAddProjectForm(Model model) {
        logger.debug("showAddProjectForm()");

        @NotNull Project project = new Project();
        // set default value
        project.setName("");
        project.setDescription("D22");
        project.setStatus(Status.Planned);
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        model.addAttribute("projectForm", project);
        populateDefaultModel(model);

        populateDefaultModel(model);

        return "projects/projectform";
    }

    // show update form
    @RequestMapping(value = "/projects/{id}/update", method = RequestMethod.GET)
    public String showUpdateProjectForm(@PathVariable("id") String id, Model model) {
        logger.debug("showUpdateProjectForm() : {}", id);

        @NotNull final Project project = serviceLocator.getProjectService().findOne(id);
        model.addAttribute("projectForm", project);

        populateDefaultModel(model);

        return "projects/projectform";
    }

    // delete project
    @RequestMapping(value = "/projects/{id}/delete", method = RequestMethod.GET)
    public String deleteProject(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes) {

        logger.debug("deleteProject() : {}", id);
        @NotNull Project project = serviceLocator.getProjectService().findOne(id);
        if (project != null)
            serviceLocator.getProjectService().remove(project);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Project is deleted!");

        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());

        return "projects/list";        
    }

    //tasks
    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
    public String saveOrUpdateTask(@ModelAttribute("taskForm") @Validated @NotNull Task task,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {

        logger.debug("saveOrUpdateTask() : {}", task);
        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "tasks/taskform";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(task.isNew()){
                redirectAttributes.addFlashAttribute("msg", "Task added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "Task updated successfully!");
            }
            serviceLocator.getTaskService().merge(task);

            populateDefaultModel(model);

            return "tasks/list";
        }
    }

    // show add task form
    @RequestMapping(value = "/tasks/add", method = RequestMethod.GET)
    public String showAddTaskForm(Model model) {

        logger.debug("showAddTaskForm()");

        @NotNull Task task = new Task();
        // set default value
        task.setName("");
        task.setDescription("D22");
        task.setProject(null);
        task.setStatus(Status.Planned);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        model.addAttribute("taskForm", task);
        populateDefaultModel(model);

        populateDefaultModel(model);

        return "tasks/taskform";
    }

    // show update form
    @RequestMapping(value = "/tasks/{id}/update", method = RequestMethod.GET)
    public String showUpdateTaskForm(@PathVariable("id") String id, Model model) {

        logger.debug("showUpdateTaskForm() : {}", id);

        @NotNull final Task task = serviceLocator.getTaskService().findOne(id);
        model.addAttribute("taskForm", task);

        populateDefaultModel(model);

        return "tasks/taskform";
    }

    // delete task
    @RequestMapping(value = "/tasks/{id}/delete", method = RequestMethod.GET)
    public String deleteTask(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes) {
        logger.debug("deleteTask() : {}", id);

        @NotNull final Task task = serviceLocator.getTaskService().findOne(id);
        if (task != null)
            serviceLocator.getTaskService().remove(task);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Task is deleted!");

        populateDefaultModel(model);

        return "tasks/list";        
    }


    private void populateDefaultModel(Model model) {
        model.addAttribute("userList", serviceLocator.getUserService().findAll());
        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());
        model.addAttribute("taskList", serviceLocator.getTaskService().findAll());
        model.addAttribute("sessionList", serviceLocator.getSessionService().findAll());

        model.addAttribute("statuses", Status.values());
        model.addAttribute("roles", RoleType.values());
    }
}
